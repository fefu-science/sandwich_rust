use sandwich::spin::Spin;
use sandwich::spin::Spin360;

#[derive(Clone)]
pub enum Plate_type {
    FerMagn,
    AntiFerMagn,
}

#[derive(Clone)]
pub struct Plate<T: Spin>{
    pub J: f64,
    pub width: usize,
    pub height: usize,
    pub depth: usize,
    pub space: Vec<Vec<Vec<T>>>,
    pub plate_type: Plate_type,
}

impl<T: Spin + Clone> Plate<T> {
    pub fn new (J:f64, width: usize, height: usize, depth:usize, plate_type: Plate_type) -> Plate<T>{
        Plate{
            J: J,
            width:  width,
            height: height,
            depth:  depth,
            space:  vec![vec![vec![T::new(); depth]; height]; width],
            plate_type: plate_type,
        }
    }
    /*pub fn get_space(&self) -> Vec<Vec<Vec<T>>>{
        self.space
    }*/
    pub fn print_spin(&self, x:usize,y:usize,z:usize){
        self.space[x][y][z].print_spin();
    }

    pub fn rotate_spin(&mut self, x:usize,y:usize,z:usize){
        &self.space[x][y][z].rotate();
    }

    pub fn rotate_spin_phi(&mut self, x:usize,y:usize,z:usize){
        &self.space[x][y][z].rotate_phi();
    }

    pub fn rotate_spin_theta(&mut self, x:usize,y:usize,z:usize){
        &self.space[x][y][z].rotate_theta();
    }

    pub fn rotate_all_spins(&mut self){
        for x in 0..self.width{
            for y in 0..self.height{
                for z in 0..self.depth{
                    &self.space[x][y][z].rotate();
                }
            }
        }
    }

    pub fn get_volume(&self) -> f64{
        self.width as f64 * self.height as f64 * self.depth as f64
    }

    pub fn get_dimensions(&self) -> (usize,usize,usize){
        (self.width, self.height, self.depth)
    }

    pub fn get_spin(&self,x:usize,y:usize,z:usize) -> T {
        self.space[x][y][z].clone()
    }

    pub fn set_spin(&mut self,x:usize,y:usize,z:usize,spin:T){
        self.space[x][y][z] = spin;
    }

    pub fn get_width(&self) -> usize{
        self.width
    }

    pub fn get_height(&self) -> usize{
        self.height
    }

    pub fn get_depth(&self) -> usize{
        self.depth
    }
}


/*impl<T: Spin + Clone> Clone for Plate<T>{
    /*fn clone(&self) -> Plate<T>{
        *self
    }*/

    fn clone(&self) -> Plate<T>{
        Plate{
            J:      self.J,
            width:  self.width,
            height: self.height,
            depth:  self.depth,
            space:  self.space,
        }
    }
}*/
