use rand;
use std;

//Интерфейс для всех видов спинов
pub trait Spin {
    fn new() -> Self;
    fn rotate(&mut self);
    fn rotate_phi(&mut self);
    fn rotate_theta(&mut self);
    fn print_spin(&self);
    //fn get_spin(&self) -> Self;
    //fn get_spin(&self) -> Self
    //fn get_value(&self);
}

//Трёхмерный спин для модели Гейзенберг
pub struct Spin360{
    pub phi: f64,
    pub theta: f64,
}

impl Spin for Spin360{
    fn new() -> Spin360{

        Spin360{
            /*phi     : 0_f64,
            theta   : 0_f64,*/
            phi     : 2_f64 * std::f64::consts::PI * rand::random::<f64>(),
            theta   : (1_f64 - 2_f64 * rand::random::<f64>()).acos(), //: std::f64::consts::PI * rand::random::<f64>(),
        }
    }

    fn rotate(&mut self){
        let PI = std::f64::consts::PI;
        self.phi = 2_f64 * PI * rand::random::<f64>();
        self.theta = (1_f64 - 2_f64 * rand::random::<f64>()).acos();
        /*self.phi = 0_f64;
        self.theta = (rand::random::<u32>() % 2) as f64 * std::f64::consts::PI;*/
    }

    fn rotate_phi(&mut self){
        let PI = std::f64::consts::PI;
        self.phi = 2_f64 * PI * rand::random::<f64>();
    }

    fn rotate_theta(&mut self){
        let PI = std::f64::consts::PI;
        self.theta = (1_f64 - 2_f64 * rand::random::<f64>()).acos();
    }

    fn print_spin(&self){
        println!("phi={} theta={}",self.phi,self.theta);
    }
}

impl Spin360{

}

impl Clone for Spin360{
    fn clone(&self) -> Spin360{
        Spin360{
            phi     : self.phi,
            theta   : self.theta,
        }
    }

}
