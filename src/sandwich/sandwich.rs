use sandwich::spin::{Spin, Spin360};
use sandwich::plate::Plate;
use sandwich::{model, plate};
use sandwich::algorithm;

pub struct Sandwich <T: Spin> {
    pub plates: Vec<Plate<T>>,
    pub size: usize,
}

impl<T: Spin + Clone> Sandwich<T> {
    pub fn new() -> Sandwich<T>{
        Sandwich{
            plates : Vec::new(),
            size : 0_usize,
        }
    }

    pub fn add_plate(&mut self, J: f64, width: usize, height: usize, depth: usize, plate_type: plate::Plate_type) {
        let new_plate: Plate<T> = Plate::new(J, width, height, depth, plate_type);
        self.plates.push(new_plate);
        self.size = self.size + 1;
    }

    pub fn size(&self) -> usize{
        //self.plates.capacity()
        self.size
    }

    pub fn print_spin(&self, plate_num: usize, x:usize,y:usize,z:usize){
        self.plates[plate_num].print_spin(x,y,z);
    }

    pub fn get_plates(&self) -> & Vec<Plate<T>>{
        & self.plates
    }

    pub fn get_mut_plates(&mut self) -> &mut Vec<Plate<T>>{
        &mut self.plates
    }

    //pub fn calc_energy(&self, plate_num: usize, x:usize,y:usize,z:usize);
}
