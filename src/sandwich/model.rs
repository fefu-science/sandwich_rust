use sandwich::spin::{Spin, Spin360};
use sandwich::sandwich::Sandwich;
use sandwich::plate::Plate;
use std::error::Error;
use std::io::prelude::*;
use std::fs::File;
use std::path::Path;
use rand;
use std;

extern crate time;
use self::time::PreciseTime;
use std::time::SystemTime;

pub enum Model{
    Heisenberg,
    Ising,
    XY,
}

pub struct Heisenberg {
    sandwich: Sandwich<Spin360>,
}
pub struct Ising {}
pub struct XY {}



impl Heisenberg{
    pub fn new(sandwich: Sandwich<Spin360>) -> Heisenberg{
        Heisenberg {
            sandwich: sandwich,
        }
    }

    pub fn run_metropolis(&mut self){
        println!("Run Metropolis for Heisenberg");
        let T_start = 0.001_f64;
        let T_finish = 20.1_f64;
        let mut T_step = 0.05;
        let mk_steps = 10_000;
        let count_mks = 20;
        let count_mks_inner_loop = 20;

        let mut T_current = T_start;
        let k_b = 1_f64;
        let mut full_energy = 0_f64;
        //let mut full_energy_average_tmp = 0_f64;
        let mut volume = 0_f64;
        let mut count_layers = 0;

        for num in 0..self.sandwich.size(){
            let current_plate = & self.sandwich.get_plates()[num as usize];
            let volume_tmp = current_plate.get_volume() as f64;
            volume += volume_tmp as f64;
            count_layers += 1;
            //println!("volume_tmp = {}",volume);
        }

        let ep1 = "./E(T)_";
        let ep2 = "./M(T)_";
        let ep3 = "./C(T)_";
        let ep4 = "./Check_parametrs_";
        let ep = "_layers.txt";
        let pth1 =  format!("{}{}{}", ep1,count_layers,ep);
        let pth2 =  format!("{}{}{}", ep2,count_layers,ep);
        let pth3 =  format!("{}{}{}", ep3,count_layers,ep);
        let pth4 =  format!("{}{}{}", ep4,count_layers,ep);

        let path1 = Path::new(&pth1);
        let path2 = Path::new(&pth2);
        let path3 = Path::new(&pth3);
        let path4 = Path::new(&pth4);
        let display = path1.display();

       // Open a file in write-only mode, returns `io::Result<File>`
        let mut file1 = File::create(&path1).unwrap();
        let mut file2 = File::create(&path2).unwrap();
        let mut file3 = File::create(&path3).unwrap();
        let mut file4 = File::create(&path4).unwrap();

        /*self.sandwich.get_mut_plates()[0].rotate_all_spins();
        self.sandwich.get_mut_plates()[1].rotate_all_spins();*/
        println!("spin 1x1x1 before");
        //self.sandwich.get_mut_plates()[0].print_spin(1,1,1);
        let mut energy_one = self.calc_energy(0, 0, 0, 0);
        println!("B 1 energy for 1x1x1 = {}",energy_one);
        println!("sum E {}", self.calc_full_energy());
        println!("sum E {}", self.calc_full_energy());
        let (sx,sy,sz) = self.calc_sum_all_spins(0);
        println!("calc sum spins {} {} {}", sx,sy,sz);
        println!("spin 1x1x1 after");
        self.sandwich.get_mut_plates()[0].rotate_spin(0,0,0);
        println!("sum E2 {}", self.calc_full_energy());
        //self.sandwich.get_mut_plates()[0].print_spin(0,1,1);
        energy_one = self.calc_energy(0, 0, 0, 0);
        println!("A 1 energy for 1x1x1 = {}",energy_one);

        file1.write_all("T \tE \tE_per_spin \r\n".as_bytes() ).unwrap();
        file2.write_all("T \tMx \tMy \tMz \tM_sqrt \r\n".as_bytes()).unwrap();
        file3.write_all("T \tC \t\tC_sqrt \tsigma_E \r\n".as_bytes() ).unwrap();
        file4.write_all("mk_steps \tcount_mks \tcount_mks_inner_loop \r\n".as_bytes() ).unwrap();
        file4.write_all( (format!("{} \t{} \t{}  \r\n", mk_steps, count_mks, count_mks_inner_loop).to_string()).as_bytes() ).unwrap();
        file4.write_all("T \ti \tM_sqrt \tE \r\n".as_bytes() ).unwrap();

        let start = PreciseTime::now();
        let mut tmp_T = 0;
        let mut full_energy_average_1 = 0_f64;

        while T_current < T_finish {

            if (T_current > 0.1){
                T_step = 0.5;
            }
            /*if (T_current > 1.2 && T_current < 1.6){
                T_step = 0.02;
            }*/

            file4.write_all( (format!("{:.6}  ",T_current).to_string()).as_bytes() ).unwrap();

            println!("!!!\n");

            let mut full_energy_average_11 = 0_f64;
            let mut full_energy_average_22 = 0_f64;
            let mut E_vec = Vec::new();
            let mut sqrt_magn = 0_f64;
            let mut fm_x = 0_f64;
            let mut fm_y = 0_f64;
            let mut fm_z = 0_f64;

            for i in 0..mk_steps{
                for random_num_plate in 0..self.sandwich.size(){
                    for random_x in 0..self.sandwich.get_plates()[random_num_plate].get_width(){
                       for random_y in 0..self.sandwich.get_plates()[random_num_plate].get_height(){
                            for random_z in 0..self.sandwich.get_plates()[random_num_plate].get_depth(){
                        //    let random_num_plate = rand::random::<usize>() % (self.sandwich.size());
                        //    let random_x = rand::random::<usize>() % (self.sandwich.get_plates()[random_num_plate].get_width());
                        //    let random_y = rand::random::<usize>() % (self.sandwich.get_plates()[random_num_plate].get_height());
                        //    let random_z = rand::random::<usize>() % (self.sandwich.get_plates()[random_num_plate].get_depth());
                            self.metropolis(T_current,random_num_plate,random_x,random_y,random_z);
                            }
                        }
                    }
                }
            }

            println!("!!!!");

            for i in 0..count_mks{
                for k in 0..count_mks_inner_loop{
                    for random_num_plate in 0..self.sandwich.size(){
                        for random_x in 0..self.sandwich.get_plates()[random_num_plate].get_width(){
                            for random_y in 0..self.sandwich.get_plates()[random_num_plate].get_height(){
                                for random_z in 0..self.sandwich.get_plates()[random_num_plate].get_depth(){
                                    self.metropolis(T_current,random_num_plate,random_x,random_y,random_z);
                                }
                            }
                        }
                    }
                }

                let full_energy_average_tmp = self.calc_full_energy();
                E_vec.push(full_energy_average_tmp);
                full_energy_average_11 += full_energy_average_tmp;
                full_energy_average_22 += full_energy_average_tmp * full_energy_average_tmp;

                //full magnitization
                fm_x = 0_f64;
                fm_y = 0_f64;
                fm_z = 0_f64;
                let mut volume = 0_f64;
                for num in 0..self.sandwich.size(){
                    let (x,y,z) = self.calc_magnetization(num);
                    let current_plate = & self.sandwich.get_plates()[num];
                    volume += current_plate.get_volume();
                    fm_x += x;
                    fm_y += y;
                    fm_z += z;
                }
                file4.write_all( (format!("Volume = {}  \r\n",volume).to_string()).as_bytes() ).unwrap();
                fm_x /= volume;
                fm_y /= volume;
                fm_z /= volume;

                sqrt_magn += (fm_x*fm_x + fm_y*fm_y + fm_z*fm_z).sqrt();

                if(i%25==0){
                    println!("i = {:?}, m_sqrt = {}, E_f_tmp = {}", i,(fm_x*fm_x + fm_y*fm_y + fm_z*fm_z).sqrt(),full_energy_average_tmp);
                    file4.write_all( (format!("i= {}  m_q= {:.10}  e= {:.10} \r\n",i,(fm_x*fm_x + fm_y*fm_y + fm_z*fm_z).sqrt(),full_energy_average_tmp).to_string()).as_bytes() ).unwrap();
                }
            }

            println!("{:?}", E_vec.len());

            sqrt_magn /= count_mks as f64;

            let mut sigma_e = 0_f64;
            full_energy_average_11 /= (count_mks as f64 );

            for i in 0..count_mks{
                sigma_e += (E_vec[i] - full_energy_average_11).powf(2_f64);
            }
            sigma_e /= count_mks as f64;
            sigma_e = sigma_e.sqrt();
            E_vec.clear();
            println!("{:?}", E_vec.len());
            println!("!!!222!!!sigma_e = {}",sigma_e);

            full_energy = full_energy_average_11;

            let mut heat_capacity = 0_f64;
            if(tmp_T == 0){
                full_energy_average_1 = full_energy_average_11;
                tmp_T +=1;
            }
            else{
                //tmp_T = 0;
                let mut full_energy_average_2 = full_energy_average_11;
                heat_capacity = (full_energy_average_2 - full_energy_average_1) / T_step;
                heat_capacity /= volume;
                full_energy_average_1 = full_energy_average_2;
            }

            let mut heat_capacity_sqrt = 0_f64;

            full_energy_average_11 = full_energy_average_11*full_energy_average_11;
            full_energy_average_22 /= (count_mks as f64 );

            heat_capacity_sqrt = (full_energy_average_22 - full_energy_average_11)  / (k_b * k_b * T_current * T_current);
            heat_capacity_sqrt /= volume;

            println!("F_a_22 = {} F_a_11 = {}", full_energy_average_22, full_energy_average_11);
            println!("C = {} C_sqrt = {}", heat_capacity, heat_capacity_sqrt);

            file1.write_all( (format!("{:.6}  {:.10}  {:.10}\r\n",T_current, full_energy, full_energy/volume).to_string()).as_bytes() ).unwrap();
            file2.write_all( (format!("{:.6}  {:.6}  {:.6}  {:.6}  {:.6}\r\n",T_current, fm_x, fm_y, fm_z, sqrt_magn).to_string()).as_bytes() ).unwrap();
            file3.write_all( (format!("{:.6}  {:.10}  {:.10} {:.10}\r\n",T_current, heat_capacity, heat_capacity_sqrt, sigma_e).to_string()).as_bytes() ).unwrap();

            println!("T = {:.6} ; E = {}", T_current, full_energy);

            T_current += T_step;

            let end = PreciseTime::now();
            println!("{} seconds", start.to(end)); //Time of execution!!!
        }
    }

// phi and theta both rotate!!!!!!
    //fn metropolis(&mut self, Temperature :f64){
    /*fn metropolis(&mut self, Temperature :f64, random_num_plate:usize, random_x:usize, random_y:usize,random_z:usize){

        //1.Выбираем случайный спин
//        let random_num_plate = rand::random::<usize>() % (self.sandwich.size());
//
//        let random_x = rand::random::<usize>() % (self.sandwich.get_plates()[random_num_plate].get_width());
//        let random_y = rand::random::<usize>() % (self.sandwich.get_plates()[random_num_plate].get_height());
//        let random_z = rand::random::<usize>() % (self.sandwich.get_plates()[random_num_plate].get_depth());
        //2.Сохраняем спин
        let old_spin = self.sandwich.get_plates()[random_num_plate].get_spin(random_x,random_y,random_z);
        //println!("cur p{} x{} y{} z{}",random_num_plate, random_x,random_y,random_z );
        //3.Вычисляем энергию
        let energy_old = self.calc_energy(random_num_plate,random_x,random_y,random_z);
        //println!("Eold={:?}", energy_old);
        //4.Переворачиваем спин
        self.sandwich.get_mut_plates()[random_num_plate].rotate_spin(random_x,random_y,random_z);
        //5.Вычисляем новую энергию
        let energy_new = self.calc_energy(random_num_plate,random_x,random_y,random_z);
        //println!("Enew={:?}", energy_new);

        let k_b = 1_f64;
        if(energy_new >= energy_old){
            let prob    = rand::random::<f64>();
            let mut prob_e = ( -(energy_new - energy_old) / (k_b * Temperature) );
            //println!("energy_new {}, energy_old {}, delta {} Temperature{}", energy_new, energy_old, energy_new - energy_old, Temperature);
            //println!("1 prob_e {}", prob_e);
            prob_e = prob_e.exp();
            //println!("2 prob_e {}", prob_e);
            //println!("prob_e = {}, prob = {}", prob_e, prob);
            if(prob > prob_e){
                //Новый спин неудачник, возвращаем старый
                self.sandwich.get_mut_plates()[random_num_plate].set_spin(random_x,random_y,random_z, old_spin);
            }
        }
    }
*/

// phi and theta separate rotate!!!!!!

    fn metropolis(&mut self, Temperature :f64, random_num_plate:usize, random_x:usize, random_y:usize,random_z:usize){

            //1.Выбираем случайный спин
    //        let random_num_plate = rand::random::<usize>() % (self.sandwich.size());
    //
    //        let random_x = rand::random::<usize>() % (self.sandwich.get_plates()[random_num_plate].get_width());
    //        let random_y = rand::random::<usize>() % (self.sandwich.get_plates()[random_num_plate].get_height());
    //        let random_z = rand::random::<usize>() % (self.sandwich.get_plates()[random_num_plate].get_depth());
            //2.Сохраняем спин
            let old_spin = self.sandwich.get_plates()[random_num_plate].get_spin(random_x,random_y,random_z);
            let old_spin_phi = old_spin.phi;
            let old_spin_theta = old_spin.theta;
            //println!("cur p{} x{} y{} z{}",random_num_plate, random_x,random_y,random_z );
            //3.Вычисляем энергию
            let energy_old = self.calc_energy(random_num_plate,random_x,random_y,random_z);
            //println!("Eold={:?}", energy_old);
            //4.Переворачиваем спин
            self.sandwich.get_mut_plates()[random_num_plate].rotate_spin_phi(random_x,random_y,random_z);

            let new_spin = self.sandwich.get_plates()[random_num_plate].get_spin(random_x,random_y,random_z);
            let new_spin_phi = new_spin.phi;
            //5.1 Вычисляем новую энергию для phi
            let mut energy_new = self.calc_energy(random_num_plate,random_x,random_y,random_z);
            //println!("Enew={:?}", energy_new);

            let k_b = 1_f64;
            if(energy_new >= energy_old){
                let prob    = rand::random::<f64>();
                let mut prob_e = ( -(energy_new - energy_old) / (k_b * Temperature) );
                //println!("energy_new {}, energy_old {}, delta {} Temperature{}", energy_new, energy_old, energy_new - energy_old, Temperature);
                //println!("1 prob_e {}", prob_e);
                prob_e = new_spin_phi.sin() / old_spin_phi.sin() * prob_e.exp();
                //println!("2 prob_e {}", prob_e);
                //println!("prob_e = {}, prob = {}", prob_e, prob);
                if(prob > prob_e){
                    //Новый спин неудачник, возвращаем старый
                    self.sandwich.get_mut_plates()[random_num_plate].set_spin(random_x,random_y,random_z, old_spin.clone());
                }
            }

            self.sandwich.get_mut_plates()[random_num_plate].rotate_spin_theta(random_x,random_y,random_z);
            let new_spin_theta = new_spin.theta;
            //5.2 Вычисляем новую энергию для theta
            energy_new = self.calc_energy(random_num_plate,random_x,random_y,random_z);

            if(energy_new >= energy_old){
                let prob    = rand::random::<f64>();
                let mut prob_e = ( -(energy_new - energy_old) / (k_b * Temperature) );
                prob_e = new_spin_theta.sin() / old_spin_theta.sin() * prob_e.exp();
                if(prob > prob_e){
                    //Новый спин неудачник, возвращаем старый
                    self.sandwich.get_mut_plates()[random_num_plate].set_spin(random_x,random_y,random_z, old_spin.clone());
                }
            }
        }


    fn calc_magnetization(&mut self, num_plate: usize) -> (f64,f64,f64){
        let current_plate = & self.sandwich.get_plates()[num_plate];
        let (x,y,z) = current_plate.get_dimensions();
        let mut sum_spin_x = 0_f64;
        let mut sum_spin_y = 0_f64;
        let mut sum_spin_z = 0_f64;

        for i in 0..x{
            for j in 0..y{
                for k in 0..z{
                    //let spin = current_plate.get_spin(i,j,k);
                    sum_spin_x += current_plate.space[i][j][k].theta.sin() * current_plate.space[i][j][k].phi.cos();
                    sum_spin_y += current_plate.space[i][j][k].theta.sin() * current_plate.space[i][j][k].phi.sin();
                    sum_spin_z += current_plate.space[i][j][k].theta.cos();
                }
            }
        }
        let spin_x_average = sum_spin_x;
        let spin_y_average = sum_spin_y;
        let spin_z_average = sum_spin_z;

        (spin_x_average, spin_y_average, spin_z_average)
    }

    fn calc_sum_all_spins(&self, num_plate: usize) -> (f64, f64, f64) {
        let mut sum_spin_x = 0_f64;
        let mut sum_spin_y = 0_f64;
        let mut sum_spin_z = 0_f64;

        let current_plate =  &self.sandwich.get_plates()[num_plate];

        for i in 0..current_plate.width{
            for j in 0..current_plate.height{
                for k in 0..current_plate.depth{
                    sum_spin_x += current_plate.space[i][j][k].theta.sin() * current_plate.space[i][j][k].phi.cos();
                    sum_spin_y += current_plate.space[i][j][k].theta.sin() * current_plate.space[i][j][k].phi.sin();
                    sum_spin_z += current_plate.space[i][j][k].theta.cos();
                }
            }
        }

        (sum_spin_x, sum_spin_y, sum_spin_z)
    }

    fn calc_energy(&mut self, num_plate:usize, x:usize, y:usize, z:usize) -> f64{
        //var for final result
        let mut energy = 0_f64;
        let plates = &self.sandwich.plates;
        let current_plate = &plates[num_plate];

        let spin_x = current_plate.space[x][y][z].theta.sin() * current_plate.space[x][y][z].phi.cos();
        let spin_y = current_plate.space[x][y][z].theta.sin() * current_plate.space[x][y][z].phi.sin();
        let spin_z = current_plate.space[x][y][z].theta.cos();

        let left    = (x + current_plate.width - 1) % current_plate.width;
        let right   = (x + 1) % current_plate.width;
        let top     = (z + current_plate.depth - 1) % current_plate.depth;
        let bot     = (z + 1) % current_plate.depth;
        let back    = (y + current_plate.height - 1) % current_plate.height;
        let front   = (y + 1) % current_plate.height;

        let mut sum_spin_x = 0_f64;
        let mut sum_spin_y = 0_f64;
        let mut sum_spin_z = 0_f64;
        //left
        sum_spin_x += current_plate.space[left][y][z].theta.sin() * current_plate.space[left][y][z].phi.cos();
        sum_spin_y += current_plate.space[left][y][z].theta.sin() * current_plate.space[left][y][z].phi.sin();
        sum_spin_z += current_plate.space[left][y][z].theta.cos();

        //right
        sum_spin_x += current_plate.space[right][y][z].theta.sin() * current_plate.space[right][y][z].phi.cos();
        sum_spin_y += current_plate.space[right][y][z].theta.sin() * current_plate.space[right][y][z].phi.sin();
        sum_spin_z += current_plate.space[right][y][z].theta.cos();
        //botom
        if( z != current_plate.depth -1 ){
            sum_spin_x += current_plate.space[x][y][bot].theta.sin() * current_plate.space[x][y][bot].phi.cos();
            sum_spin_y += current_plate.space[x][y][bot].theta.sin() * current_plate.space[x][y][bot].phi.sin();
            sum_spin_z += current_plate.space[x][y][bot].theta.cos();
        }

        //top
        if( z != 0){
            sum_spin_x += current_plate.space[x][y][top].theta.sin() * current_plate.space[x][y][top].phi.cos();
            sum_spin_y += current_plate.space[x][y][top].theta.sin() * current_plate.space[x][y][top].phi.sin();
            sum_spin_z += current_plate.space[x][y][top].theta.cos();
        }

        //back
        sum_spin_x += current_plate.space[x][back][z].theta.sin() * current_plate.space[x][back][z].phi.cos();
        sum_spin_y += current_plate.space[x][back][z].theta.sin() * current_plate.space[x][back][z].phi.sin();
        sum_spin_z += current_plate.space[x][back][z].theta.cos();
        //front
        sum_spin_x += current_plate.space[x][front][z].theta.sin() * current_plate.space[x][front][z].phi.cos();
        sum_spin_y += current_plate.space[x][front][z].theta.sin() * current_plate.space[x][front][z].phi.sin();
        sum_spin_z += current_plate.space[x][front][z].theta.cos();
        energy += (-current_plate.J) * (spin_x * sum_spin_x + spin_y * sum_spin_y + spin_z * sum_spin_z);
        //Получаем пластинку, которая выше
        if(num_plate != 0){
            let plate_up = &plates[num_plate-1];
            let (sum_top_spins_x, sum_top_spins_y, sum_top_spins_z) = self.calc_sum_all_spins(num_plate-1);
            let mut Jbcat = 0_f64; //J between current and top plate

            if(plate_up.J.abs() > current_plate.J.abs()){
                Jbcat = -0.3*(plate_up.J);// / 10_f64);
            } else {
                Jbcat = -0.3*(current_plate.J);// / 10_f64);
            }
            energy += (-Jbcat) * (spin_x * sum_top_spins_x + spin_y * sum_top_spins_y + spin_z * sum_top_spins_z);
        }
        //Получаем пластинку, которая ниже
        if(num_plate != self.sandwich.size() -1 ){
            let plate_down = &&plates[num_plate+1];
            let (sum_bot_spins_x, sum_bot_spins_y, sum_bot_spins_z) = self.calc_sum_all_spins(num_plate+1);
            let mut Jbcab = 0_f64; //J between current and bottom plate

            if(plate_down.J.abs() > current_plate.J.abs()){
                Jbcab = -0.3*(plate_down.J);// / 10_f64);
            } else {
                Jbcab = -0.3*(current_plate.J);// / 10_f64);
            }
            energy += (-Jbcab) * (spin_x * sum_bot_spins_x + spin_y * sum_bot_spins_y + spin_z * sum_bot_spins_z);
        }
        energy
    }

    fn calc_full_energy(&mut self) ->f64{
        let mut full_energy = 0_f64;
        for plate in 0..self.sandwich.size(){
            for x in 0..self.sandwich.get_plates()[plate].get_width(){
                for y in 0..self.sandwich.get_plates()[plate].get_height(){
                    for z in 0..self.sandwich.get_plates()[plate].get_depth(){

                        full_energy += self.calc_energy(plate,x,y,z);
                    }
                }
            }
        }
        full_energy / 2_f64
    }
}
