extern crate rand;
mod sandwich;
use sandwich::sandwich::Sandwich;
use sandwich::spin::Spin360;
use sandwich::{plate, model, algorithm};
use sandwich::plate::Plate_type;

fn main() {
    println!("Sandwich run!");

    let mut sandwich: Sandwich<Spin360> = Sandwich::new();

    //Добавляем
    /*sandwich.add_plate(2_f64, 3, 3, 3, Plate_type::FerMagn);
    sandwich.add_plate(2_f64, 3, 3, 3, Plate_type::FerMagn);
    sandwich.add_plate(2_f64, 3, 3, 3, Plate_type::FerMagn);*/
    //sandwich.add_plate(2_f64, 2, 2, 1, Plate_type::FerMagn);
    sandwich.add_plate(1_f64, 6, 6, 3, Plate_type::FerMagn);
    sandwich.add_plate(1_f64, 6, 6, 3, Plate_type::FerMagn);
    sandwich.add_plate(1_f64, 6, 6, 3, Plate_type::FerMagn);
    //sandwich.add_plate(1_f64, 2, 2, 1, Plate_type::FerMagn);
    //sandwich.add_plate(1_f64, 2, 2, 1, Plate_type::FerMagn);
    //sandwich.add_plate(1_f64, 3, 3, 3, Plate_type::FerMagn);
//    sandwich.add_plate(1_f64, 10, 10, 3, Plate_type::FerMagn);
//    sandwich.add_plate(1_f64, 10, 10, 3, Plate_type::FerMagn);
//    let test = -0.06_f64;
//    println!("exp = {:?}", test.exp());
    let mut model = model::Heisenberg::new(sandwich);
    model.run_metropolis();


    sandwich.add_plate(1_f64, 10, 10, 3, Plate_type::FerMagn);
    sandwich.add_plate(0_f64, 10, 10, 2, Plate_type::NoneMagn);
    sandwich.add_plate(1_f64, 10, 10, 3, Plate_type::FerMagn);
    sandwich.add_plate(0_f64, 10, 10, 2, Plate_type::NoneMagn);
    sandwich.add_plate(1_f64, 10, 10, 3, Plate_type::FerMagn);

    let mut model = model::Heisenberg::new(sandwich);
    model.run_metropolis();
    println!("End of calculations");
}
